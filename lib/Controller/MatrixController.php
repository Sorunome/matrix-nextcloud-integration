<?php
namespace OCA\MatrixIntegration\Controller;

use OCP\AppFramework\Http\DataDisplayResponse;
use OCA\MatrixIntegration\MatrixClient;
use OCA\MatrixIntegration\Controller\FileShareController;
use OCA\MatrixIntegration\Db\AccountDataMapper;
use OCA\MatrixIntegration\Db\RoomAccountDataMapper;
use OCA\MatrixIntegration\Db\RoomMapper;
use OCA\MatrixIntegration\Db\RoomStateMapper;
use OCA\MatrixIntegration\Service\RoomSyncService;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use OCP\IConfig;
use OCP\IRequest;
use OCP\IUserSession;

class MatrixController extends Controller {
	private $config;
	private $userSession;
	private $matrixClient;
	private $accountDataMapper;
	private $roomAccountDataMapper;
	private $roomMapper;
	private $roomStateMapper;
	private $roomSyncService;
	private $fileShareController;
	protected $appName;

	public function __construct(
		string $appName,
		IRequest $request,
		IConfig $config,
		IUserSession $userSession,
		AccountDataMapper $accountDataMapper,
		RoomAccountDataMapper $roomAccountDataMapper,
		RoomMapper $roomMapper,
		RoomStateMapper $roomStateMapper,
		RoomSyncService $roomSyncService,
		FileShareController $fileShareController,
	) {
		parent::__construct($appName, $request);
		$this->appName = $appName;
		$this->config = $config;
		$this->userSession = $userSession;
		$this->accountDataMapper = $accountDataMapper;
		$this->roomAccountDataMapper = $roomAccountDataMapper;
		$this->roomMapper = $roomMapper;
		$this->roomStateMapper = $roomStateMapper;
		$this->roomSyncService = $roomSyncService;
		$this->fileShareController = $fileShareController;
	}

	private function getUserValue($key, $default = '') {
		return $this->config->getUserValue($this->userSession->getUser()->getUID(), $this->appName, $key, $default);
	}

	private function setUserValue($key, $value) {
		return $this->config->setUserValue($this->userSession->getUser()->getUID(), $this->appName, $key, $value ?? '');
	}

	private function getMatrixClient() {
		if (!$this->matrixClient) {
			$accessToken = $this->getUserValue('access_token');
			$homeserverUrl = $this->getUserValue('homeserver_url');
			$this->matrixClient = new MatrixClient($homeserverUrl, $accessToken);
		}
		return $this->matrixClient;
	}

	private function clearUser() {
		$this->setUserValue('access_token', NULL);
		$this->setUserValue('homeserver_url', NULL);
		$this->setUserValue('room_sync_since', NULL);
		$userId = $this->userSession->getUser()->getUID();
		$this->accountDataMapper->deleteAll($userId);
		$this->roomAccountDataMapper->deleteAll($userId);
		$this->roomMapper->deleteAll($userId);
		$this->roomStateMapper->deleteAll($userId);
	}

	/**
	 * @NoAdminRequired
	 * @param string $username
	 * @param string $password
	 */
	public function login($username, $password) {
		$cl = $this->getMatrixClient();
		$cl->logout();
		if ($cl->login($username, $password)) {
			$this->setUserValue('access_token', $cl->getAccessToken());
			$this->setUserValue('homeserver_url', $cl->getHomeserverUrl());
		} else {
			$this->clearUser();
		}
		return $this->whoami();
	}

	/**
	 * @NoAdminRequired
	 */
	public function logout() {
		$this->getMatrixClient()->logout();
		$this->clearUser();
		return $this->whoami();
	}

	/**
	 * @NoAdminRequired
	 */
	public function whoami() {
		$cl = $this->getMatrixClient();
		if (!$cl->getAccessToken()) {
			return new JSONResponse(['logged_in' => false]);
		}
		return new JSONResponse([
			'logged_in' => true,
			'user_id' => $cl->getUserId(),
		]);
	}

	/**
	 * @NoAdminRequired
	 */
	public function roomSummary() {
		return new JSONResponse(array_map(function ($r) {
			return [
				'room_id' => $r->getRoomId(),
				'display_name' => $r->getEffectiveName(),
				'avatar_url' => $r->getEffectiveAvatar(),
				'topic' => $r->getEffectiveTopic(),
			];
		}, $this->roomMapper->getAll($this->userSession->getUser()->getUID())));
	}
	
	/**
	 * @NoCSRFRequired
	 * @NoAdminRequired
	 * @param string $mxc
	 */
	public function thumbnail($mxc) {
		$cl = $this->getMatrixClient();
		$ch = $cl->setupRawRequest('/_matrix/media/r0/thumbnail/' . $mxc . '?width=10&height=10&method=scale');
		$result = curl_exec($ch);
		$info = curl_getinfo($ch, CURLINFO_CONTENT_TYPE | CURLINFO_RESPONSE_CODE);		
		curl_close($ch);
		if ($info) {
			$resp = new DataDisplayResponse($result, $info['http_code'], ['Content-Type' => $info['content_type']]);
			$resp->cacheFor(3600 * 24);
			return $resp;	
		} else {
			$resp = new DataDisplayResponse($result, 200);
			$resp->cacheFor(3600 * 24);
			return $resp;	
		}
	}
	
	/**
	 * @NoAdminRequired
	 * @param string $room_id
	 * @param string $share_id
	 */
	public function share($room_id, $share_id) {
		$cl = $this->getMatrixClient();
		$event = $this->fileShareController->getEvent($share_id, true);
		$resp = $cl->doRequest('/_matrix/client/r0/rooms/' . urlencode($room_id) . '/send/' . urlencode($event['type']) . '/' . 'nextcloud-' . time(), 'PUT', $event['content']);
		return new JSONResponse([
			'success' => true,
			'response' => $resp,
		]);
	}
}
