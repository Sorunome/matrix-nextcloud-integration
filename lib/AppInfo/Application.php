<?php
namespace OCA\MatrixIntegration\AppInfo;

use \OCA\Files\Event\LoadSidebar;
use OCP\AppFramework\App;
use OCP\EventDispatcher\IEventDispatcher;
use OCP\Util;

class Application extends App {
	public function __construct() {
		$appName = 'matrix_integration';
		parent::__construct($appName);
		/* @var IEventDispatcher $eventDispatcher */
		$dispatcher = $this->getContainer()->query(IEventDispatcher::class);
		$dispatcher->addListener(LoadSidebar::class, function(LoadSidebar $event) use ($appName) {
			Util::addScript($appName, 'common');
			Util::addStyle($appName, 'style');
		});
	}
}
