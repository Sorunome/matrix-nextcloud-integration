<?php
namespace OCA\MatrixIntegration\Db;

use OCP\AppFramework\Db\Entity;

class RoomState extends Entity {
	protected $userId;
	protected $roomId;
	protected $eventId;
	protected $originServerTs;
	protected $sender;
	protected $type;
	protected $unsigned;
	protected $content;
	protected $stateKey;

	public function __construct() {
		$this->addType('user_id', 'string');
		$this->addType('room_id', 'string');
		$this->addType('event_id', 'string');
		$this->addType('origin_server_ts', 'int');
		$this->addType('sender', 'string');
		$this->addType('type', 'string');
		$this->addType('unsigned', 'string');
		$this->addType('content', 'string');
		$this->addType('state_key', 'string');
	}

	public function jsonSetContent($c) {
		$this->setContent(json_encode($c));
	}

	public function jsonGetContent() {
		return json_decode($this->getContent(), true);
	}

	public function jsonSetUnsigned($c) {
		$this->setUnsigned(json_encode($c));
	}

	public function jsonGetUnsigned() {
		return json_decode($this->getUnsigned(), true);
	}
}
