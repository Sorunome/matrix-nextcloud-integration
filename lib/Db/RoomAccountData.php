<?php
namespace OCA\MatrixIntegration\Db;

use OCP\AppFramework\Db\Entity;

class RoomAccountData extends Entity {
	protected $userId;
	protected $roomId;
	protected $type;
	protected $content;

	public function __construct() {
		$this->addType('user_id', 'string');
		$this->addType('room_id', 'string');
		$this->addType('type', 'string');
		$this->addType('content', 'string');
	}

	public function jsonSetContent($c) {
		$this->setContent(json_encode($c));
	}

	public function jsonGetContent() {
		return json_decode($this->getContent(), true);
	}
}
