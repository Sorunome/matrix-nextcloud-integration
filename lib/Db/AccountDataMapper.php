<?php
namespace OCA\MatrixIntegration\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCA\MatrixIntegration\Db\CustomQBMapper;

class AccountDataMapper extends CustomQBMapper {
	protected $uniqueColums = ['user_id', 'type'];

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'matrix_int_acc_data');
	}
}
