<?php
namespace OCA\MatrixIntegration\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCA\MatrixIntegration\Db\CustomQBMapper;
use OCP\AppFramework\Db\DoesNotExistException;

class RoomStateMapper extends CustomQBMapper {
	protected $uniqueColums = ['user_id', 'room_id', 'type', 'state_key'];

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'matrix_int_room_state');
	}

	public function get(Room $room, $type, $stateKey = '') {
		if (!$stateKey) {
			$stateKey = '';
		}
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('user_id', $qb->createNamedParameter($room->getUserId(), IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->eq('room_id', $qb->createNamedParameter($room->getRoomId(), IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->eq('type', $qb->createNamedParameter($type, IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->eq('state_key', $qb->createNamedParameter($stateKey, IQueryBuilder::PARAM_STR))
			);
		try {
			return $this->findEntity($qb);
		} catch (DoesNotExistException $ex) {
			return NULL;
		}
	}
	
	public function getAllOfRoom(Room $room, $type) {
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('user_id', $qb->createNamedParameter($room->getUserId(), IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->eq('room_id', $qb->createNamedParameter($room->getRoomId(), IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->eq('type', $qb->createNamedParameter($type, IQueryBuilder::PARAM_STR))
			);
		try {
			return $this->findEntities($qb);
		} catch (DoesNotExistException $ex) {
			return [];
		}
	}

	public function getContent(Room $room, $type, $stateKey = '') {
		$event = $this->get($room, $type, $stateKey);
		if (!$event) {
			return NULL;
		}
		return $event->jsonGetContent();
	}

	public function getAllContent(Room $room, $type) {
		$events = $this->getAllOfRoom($room, $type);
		if (!$events) {
			return [];
		}
		$ret = [];
		foreach ($events as $event) {
			$ret[$event->getStateKey()] = $event->jsonGetContent();
		}
		return $ret;
	}
}
