<?php
namespace OCA\MatrixIntegration\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCA\MatrixIntegration\Db\CustomQBMapper;

class RoomMapper extends CustomQBMapper {
	protected $uniqueColums = ['user_id', 'room_id'];

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'matrix_int_room');
	}

	public function getAllNeedUpdate($userId) : array {
		$qb = $this->db->getQueryBuilder();
		$qb->select('*')
			->from($this->tableName)
			->where(
				$qb->expr()->eq('user_id', $qb->createNamedParameter($userId, IQueryBuilder::PARAM_STR))
			)
			->andWhere(
				$qb->expr()->orX(
					$qb->expr()->eq('name_outdated', $qb->createNamedParameter(true, IQueryBuilder::PARAM_BOOL)),
					$qb->expr()->eq('avatar_outdated', $qb->createNamedParameter(true, IQueryBuilder::PARAM_BOOL)),
					$qb->expr()->eq('topic_outdated', $qb->createNamedParameter(true, IQueryBuilder::PARAM_BOOL))
				)
			);
		return $this->findEntities($qb);
	}
}
