<?php
namespace OCA\MatrixIntegration\Db;

use OCP\AppFramework\Db\Entity;

class Room extends Entity {
	protected $userId;
	protected $roomId;
	protected $membership;
	protected $highlightCount;
	protected $notificationCount;
	protected $joinedMemberCount;
	protected $invitedMemberCount;
	protected $heroes;
	protected $effectiveName;
	protected $effectiveAvatar;
	protected $effectiveTopic;
	protected $nameOutdated;
	protected $avatarOutdated;
	protected $topicOutdated;

	public function __construct() {
		$this->addType('user_id', 'string');
		$this->addType('room_id', 'string');
		$this->addType('membership', 'string');
		$this->addType('highlight_count', 'int');
		$this->addType('notification_count', 'int');
		$this->addType('joined_member_count', 'int');
		$this->addType('invited_member_count', 'int');
		$this->addType('heroes', 'string');
		$this->addType('effective_name', 'string');
		$this->addType('effective_topic', 'string');
		$this->addType('effective_topic', 'string');
		$this->addType('name_outdated', 'boolean');
		$this->addType('avatar_outdated', 'boolean');
		$this->addType('topic_outdated', 'boolean');
	}

	public function jsonSetHeroes($h) {
		$this->setHeroes(json_encode($h));
	}

	public function jsonGetHeroes() {
		return json_decode($this->getHeroes(), true);
	}
}
