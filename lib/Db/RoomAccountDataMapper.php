<?php
namespace OCA\MatrixIntegration\Db;

use OCP\IDBConnection;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCA\MatrixIntegration\Db\CustomQBMapper;

class RoomAccountDataMapper extends CustomQBMapper {
	protected $uniqueColums = ['user_id', 'room_id', 'type'];

	public function __construct(IDBConnection $db) {
		parent::__construct($db, 'mtrx_int_rm_acc_data');
	}
}
