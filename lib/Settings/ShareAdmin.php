<?php
namespace OCA\MatrixIntegration\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\Settings\ISettings;

class ShareAdmin implements ISettings {
	private $config;
	protected $appName;

	public function __construct(
		string $appName,
		IConfig $config
	) {
		$this->appName = $appName;
		$this->config = $config;
	}

	private function getAppValue($key, $default = '') {
		return $this->config->getAppValue($this->appName, $key, $default);
	}

	private function setAppValue($key, $value) {
		$this->config->setAppValue($this->appName, $key, $value);
	}

	public function getForm() {
		$parameters = [
			'share_domain' => $this->getAppValue('share_domain', $this->config->getSystemValue('trusted_domains')[0]),
			'share_prefix' => $this->getAppValue('share_prefix'),
			'share_suffix' => $this->getAppValue('share_suffix'),
			'appName' => $this->appName,
		];
		return new TemplateResponse($this->appName, 'settings-share-admin', $parameters, '');
	}

	public function getSection() {
		return 'sharing';
	}

	public function getPriority() {
		return 70;
	}
}
