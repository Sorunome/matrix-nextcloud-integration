<?php
namespace OCA\MatrixIntegration\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\Settings\ISettings;

class Personal implements ISettings {
	private $config;
	protected $appName;

	public function __construct(
		string $appName,
		IConfig $config
	) {
		$this->appName = $appName;
		$this->config = $config;
	}

	public function getForm() {
		$parameters = [
			'appName' => $this->appName,
		];
		return new TemplateResponse($this->appName, 'settings-personal', $parameters, '');
	}

	public function getSection() {
		return 'personal-info';
	}

	public function getPriority() {
		return 70;
	}
}
