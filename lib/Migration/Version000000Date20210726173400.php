<?php
namespace OCA\MatrixIntegration\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000000Date20210726173400 extends SimpleMigrationStep {
	/**
	* @param IOutput $output
	* @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	* @param array $options
	* @return null|ISchemaWrapper
	*/
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options) {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();
		if (!$schema->hasTable('matrix_int_room')) {
			$table = $schema->createTable('matrix_int_room');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
			]);
			$table->addColumn('user_id', 'string', [
				'notnull' => true,
				'length' => 200,
			]);
			$table->addColumn('room_id', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('membership', 'text', [
				'notnull' => true,
				'default' => 'join',
			]);
			$table->addColumn('highlight_count', 'integer', [
				'notnull' => true,
				'default' => 0,
			]);
			$table->addColumn('notification_count', 'integer', [
				'notnull' => true,
				'default' => 0,
			]);
			$table->addColumn('joined_member_count', 'integer', [
				'notnull' => true,
				'default' => 0,
			]);
			$table->addColumn('invited_member_count', 'integer', [
				'notnull' => true,
				'default' => 0,
			]);
			$table->addColumn('heroes', 'text', [
				'notnull' => true,
				'default' => '[]',
			]);
			$table->addColumn('effective_name', 'text', [
				'notnull' => false,
			]);
			$table->addColumn('effective_avatar', 'text', [
				'notnull' => false,
			]);
			$table->addColumn('effective_topic', 'text', [
				'notnull' => false,
			]);
			$table->addColumn('name_outdated', 'boolean', [
				'notnull' => false,
				'default' => true,
			]);
			$table->addColumn('avatar_outdated', 'boolean', [
				'notnull' => false,
				'default' => true,
			]);
			$table->addColumn('topic_outdated', 'boolean', [
				'notnull' => false,
				'default' => true,
			]);
			$table->setPrimaryKey(['id']);
			$table->addUniqueIndex(['user_id', 'room_id'], 'matrix_int_r_ur_idx');
		}
		if (!$schema->hasTable('matrix_int_room_state')) {
			$table = $schema->createTable('matrix_int_room_state');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
			]);
			$table->addColumn('user_id', 'string', [
				'notnull' => true,
				'length' => 200,
			]);
			$table->addColumn('room_id', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('event_id', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('origin_server_ts', 'bigint', [
				'notnull' => true,
				'default' => 0,
			]);
			$table->addColumn('sender', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('type', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('unsigned', 'text', [
				'notnull' => true,
				'default' => '{}',
			]);
			$table->addColumn('content', 'text', [
				'notnull' => true,
				'default' => '{}',
			]);
			$table->addColumn('state_key', 'text', [
				'notnull' => true,
			]);
			$table->setPrimaryKey(['id']);
			$table->addUniqueIndex(['user_id', 'room_id', 'type', 'state_key'], 'matrix_int_rs_urts_idx');
		}
		if (!$schema->hasTable('matrix_int_acc_data')) {
			$table = $schema->createTable('matrix_int_acc_data');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
			]);
			$table->addColumn('user_id', 'string', [
				'notnull' => true,
				'length' => 200,
			]);
			$table->addColumn('type', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('content', 'text', [
				'notnull' => true,
				'default' => '{}',
			]);
			$table->setPrimaryKey(['id']);
			$table->addUniqueIndex(['user_id', 'type'], 'matrix_int_ad_ut_idx');
		}
		if (!$schema->hasTable('mtrx_int_rm_acc_data')) {
			$table = $schema->createTable('mtrx_int_rm_acc_data');
			$table->addColumn('id', 'integer', [
				'autoincrement' => true,
				'notnull' => true,
			]);
			$table->addColumn('user_id', 'string', [
				'notnull' => true,
				'length' => 200,
			]);
			$table->addColumn('room_id', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('type', 'text', [
				'notnull' => true,
			]);
			$table->addColumn('content', 'text', [
				'notnull' => true,
				'default' => '{}',
			]);
			$table->setPrimaryKey(['id']);
			$table->addUniqueIndex(['user_id', 'room_id', 'type'], 'matrix_int_rad_urt_idx');
		}
		return $schema;
	}
}
