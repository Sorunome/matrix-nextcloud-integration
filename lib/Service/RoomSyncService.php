<?php
namespace OCA\MatrixIntegration\Service;

use OCA\MatrixIntegration\MatrixClient;
use OCA\MatrixIntegration\Db\AccountData;
use OCA\MatrixIntegration\Db\AccountDataMapper;
use OCA\MatrixIntegration\Db\RoomAccountData;
use OCA\MatrixIntegration\Db\RoomAccountDataMapper;
use OCA\MatrixIntegration\Db\Room;
use OCA\MatrixIntegration\Db\RoomMapper;
use OCA\MatrixIntegration\Db\RoomState;
use OCA\MatrixIntegration\Db\RoomStateMapper;
use OCP\IConfig;

function is_key_type(&$arr, $key, $type) {
	return is_array($arr) && array_key_exists($key, $arr) && ('is_' . $type)($arr[$key]);
}

class RoomSyncService {
	private $appName;
	private $config;
	private $accountDataMapper;
	private $roomAccountDataMapper;
	private $roomMapper;
	private $roomStateMapper;
	private $syncFilter = [
		'presence' => [
			'limit' => 0,
			'types' => ['none'],
		],
		'room' => [
			'ephemeral' => [
				'limit' => 0,
				'types' => ['none'],
			],
			'timeline' => [
				'limit' => 0,
				'types' => ['none'],
				'lazy_load_members' => true,
			],
			'state' => [
				'lazy_load_members' => true,
				'types' => ['m.room.name', 'm.room.avatar', 'm.room.topic', 'm.room.canonical_alias'],
			],
		],
	];

	public function __construct(
		string $appName,
		IConfig $config,
		AccountDataMapper $accountDataMapper,
		RoomAccountDataMapper $roomAccountDataMapper,
		RoomMapper $roomMapper,
		RoomStateMapper $roomStateMapper
	) {
		$this->appName = $appName;
		$this->config = $config;
		$this->accountDataMapper = $accountDataMapper;
		$this->roomAccountDataMapper = $roomAccountDataMapper;
		$this->roomMapper = $roomMapper;
		$this->roomStateMapper = $roomStateMapper;
	}

	private function getUserValue($userId, $key, $default = '') {
		return $this->config->getUserValue($userId, $this->appName, $key, $default);
	}

	private function setUserValue($userId, $key, $value) {
		return $this->config->setUserValue($userId, $this->appName, $key, $value ?? '');
	}

	public function sync($userId) {
		$accessToken = $this->getUserValue($userId, 'access_token');
		$homeserverUrl = $this->getUserValue($userId, 'homeserver_url');
		if (!$accessToken || !$homeserverUrl) {
			return;
		}
		$cl = new MatrixClient($homeserverUrl, $accessToken);
		$filterId = $cl->uploadFilter($this->syncFilter);
		$since = $this->getUserValue($userId, 'room_sync_since');
		$query = [
			'set_presence' => 'offline',
			'filter' => $filterId,
		];
		if ($since) {
			$query['since'] = $since;
		}
		$response = $cl->doRequest('/_matrix/client/r0/sync?' . http_build_query($query));
		if (!is_array($response)) {
			return;
		}
		if (is_key_type($response, 'account_data', 'array') && is_key_type($response['account_data'], 'events', 'array')) {
			foreach ($response['account_data']['events'] as $event) {
				if (
					!is_key_type($event, 'type', 'string') ||
					!is_key_type($event, 'content', 'array')
				) {
					continue;
				}
				$accountData = new AccountData();
				$accountData->setUserId($userId);
				$accountData->setType($event['type']);
				$accountData->jsonSetContent($event['content']);
				$this->accountDataMapper->insertOrUpdate($accountData);
			}
		}
		if (is_key_type($response, 'rooms', 'array')) {
			foreach (['join', 'leave', 'invite', 'knock'] as $membership) {
				if (!is_key_type($response['rooms'], $membership, 'array')) {
					continue;
				}
				foreach ($response['rooms'][$membership] as $roomId => $room) {
					if (!is_string($roomId) || !is_array($room)) {
						continue;
					}
					$roomObj = new Room();
					$roomObj->setUserId($userId);
					$roomObj->setRoomId($roomId);
					$roomObj = $this->roomMapper->getExisting($roomObj);
					$roomObj->setMembership($membership);
					if (is_key_type($room, 'summary', 'array')) {
						$summary = $room['summary'];
						if (is_key_type($summary, 'm.b', 'array')) {
							$roomObj->jsonSetHeroes($summary['m.heroes']);
							$roomObj->setNameOutdated(true);
						}
						if (is_key_type($summary, 'm.joined_member_count', 'int')) {
							$roomObj->setJoinedMemberCount($summary['m.joined_member_count']);
							$roomObj->setNameOutdated(true);
						}
						if (is_key_type($summary, 'm.invited_member_count', 'int')) {
							$roomObj->setInvitedMemberCount($summary['m.invited_member_count']);
						}
					}
					if (!$roomObj->getHeroes()) {
						$roomObj->setHeroes('[]');
					}
					if (is_key_type($room, 'unread_notifications', 'array')) {
						$unread = $room['unread_notifications'];
						if (is_key_type($unread, 'highlight_count', 'int')) {
							$roomObj->setHighlightCount($unread['highlight_count']);
						}
						if (is_key_type($unread, 'notification_count', 'int')) {
							$roomObj->setNotificationCount($unread['notification_count']);
						}
					}
					if (is_key_type($room, 'account_data', 'array') && is_key_type($room['account_data'], 'events', 'array')) {
						foreach ($room['account_data']['events'] as $event) {
							if (
								!is_array($event) ||
								!is_key_type($event, 'type', 'string') ||
								!is_key_type($event, 'content', 'array')
							) {
								continue;
							}
							$roomAccountData = new RoomAccountData();
							$roomAccountData->setUserId($userId);
							$roomAccountData->setRoomId($roomId);
							$roomAccountData->setType($event['type']);
							$roomAccountData->jsonSetContent($event['content']);
							$this->roomAccountDataMapper->insertOrUpdate($roomAccountData);
						}
					}
					foreach (['invite_state', 'state', 'timeline'] as $stateSource) {
						if (!is_key_type($room, $stateSource, 'array') || !is_key_type($room[$stateSource], 'events', 'array')) {
							continue;
						}
						foreach ($room[$stateSource]['events'] as $event) {
							if (
								!is_key_type($event, 'type', 'string') ||
								!is_key_type($event, 'state_key', 'string') ||
								!is_key_type($event, 'sender', 'string') ||
								!is_key_type($event, 'content', 'array') ||
								!is_key_type($event, 'event_id', 'string')
							) {
								continue;
							}
							if (in_array($event['type'], ['m.room.name', 'm.room.member', 'm.room.canonical_alias'])) {
								$roomObj->setNameOutdated(true);
							}
							if (in_array($event['type'], ['m.room.avatar', 'm.room.member'])) {
								$roomObj->setAvatarOutdated(true);
							}
							if (in_array($event['type'], ['m.room.topic'])) {
								$roomObj->setTopicOutdated(true);
							}
							$roomState = new RoomState();
							$roomState->setUserId($userId);
							$roomState->setRoomId($roomId);
							$roomState->setEventId($event['event_id']);
							if (is_key_type($event, 'origin_server_ts', 'int')) {
								$roomState->setOriginServerTs($event['origin_server_ts']);
							}
							$roomState->setSender($event['sender']);
							$roomState->setType($event['type']);
							$roomState->jsonSetUnsigned(is_key_type($event, 'unsigned', 'array') ? $event['unsigned'] : []);
							$roomState->jsonSetContent($event['content']);
							$roomState->setStateKey($event['state_key']);
							$this->roomStateMapper->insertOrUpdate($roomState);
						}
					}
					$this->roomMapper->insertOrUpdate($roomObj);
				}
			}
		}
		if (is_key_type($response, 'next_batch', 'string')) {
			$this->setUserValue($userId, 'room_sync_since', $response['next_batch']);
		}
		return $cl;
	}

	public function updateCache($userId, $cl) {
		$rooms = $this->roomMapper->getAllNeedUpdate($userId);
		foreach ($rooms as $room) {
			if ($room->getNameOutdated()) {
				// name outdated
				$event = $this->roomStateMapper->getContent($room, 'm.room.name');
				if (is_key_type($event, 'name', 'string') && $event['name']) {
					$room->setEffectiveName($event['name']);
				} else {
					$event = $this->roomStateMapper->getContent($room, 'm.room.canonical_alias');
					if (is_key_type($event, 'alias', 'string') && $event['alias']) {
						$room->setEffectiveName($event['alias']);
					} else {
						// ok....time to go off of heroes
						$heroes = $room->jsonGetHeroes();
						$heroeNames = [];
						$totalMembers = $room->getJoinedMemberCount() + $room->getInvitedMemberCount();
						foreach ($heroes as $hero) {
							if (!is_string($hero) || $hero === $cl->getUserId()) {
								continue;
							}
							$event = $this->roomStateMapper->getContent($room, 'm.room.member', $hero);
							if (is_key_type($event, 'displayname', 'string')) {
								$heroeNames[] = $event['displayname'];
							} else {
								$heroeNames[] = explode('@', explode(':', $hero, 2)[0], 2)[1];
							}
						}
						if (sizeof($heroeNames) === 0 && $totalMembers < 6) {
							$events = $this->roomStateMapper->getAllContent($room, 'm.room.member');
							foreach ($events as $mxid => $event) {
								if ($mxid === $cl->getUserId()) {
									continue;
								}								
								if (is_key_type($event, 'displayname', 'string')) {
									$heroeNames[] = $event['displayname'];
								} else {
									$heroeNames[] = explode('@', explode(':', $mxid, 2)[0], 2)[1];
								}
							}
						}
						var_dump($heroeNames);
						if (sizeof($heroeNames) === 0) {
							$room->setEffectiveName('Empty Room');
						} else {
							$totalHeroes = sizeof($heroeNames);
							$nameStr = implode(', ', $heroeNames);
							if ($totalMembers - 1 > $totalHeroes && $totalMembers > 1) {
								$room->setEffectiveName($nameStr . ' and ' . ($totalMembers - $totalHeroes) . ' others');
							} else {
								$room->setEffectiveName($nameStr);
							}
						}
					}
				}
				$room->setNameOutdated(0);
			}
			if ($room->getAvatarOutdated()) {
				// avatar outdated
				$event = $this->roomStateMapper->getContent($room, 'm.room.avatar');
				if (is_key_type($event, 'url', 'string')) {
					$room->setEffectiveAvatar($event['url']);
				} else {
					$heroes = $room->jsonGetHeroes();
					$totalMembers = $room->getJoinedMemberCount() + $room->getInvitedMemberCount();
					$avatar = '';
					foreach ($heroes as $hero) {
						if (!is_string($hero) || $hero === $cl->getUserId()) {
							continue;
						}
						$event = $this->roomStateMapper->getContent($room, 'm.room.member', $hero);
						if (is_key_type($event, 'avatar_url', 'string') && str_starts_with($event['avatar_url'], 'mxc://')) {
							$avatar = $event['avatar_url'];
							break;
						}
					}
					if (!$avatar && $totalMembers < 6) {
						$events = $this->roomStateMapper->getAllContent($room, 'm.room.member');
						foreach ($events as $mxid => $event) {
							if ($mxid === $cl->getUserId()) {
								continue;
							}
							if (is_key_type($event, 'avatar_url', 'string') && str_starts_with($event['avatar_url'], 'mxc://')) {
								$avatar = $event['avatar_url'];
								break;
							}
						}
					}
					$room->setEffectiveAvatar($avatar);
				}
				$room->setAvatarOutdated(0);
			}
			if ($room->getTopicOutdated()) {
				// topic outdated
				$event = $this->roomStateMapper->getContent($room, 'm.room.topic');
				$room->setEffectiveTopic(is_key_type($event, 'topic', 'string') ? $event['topic'] : '');
				$room->setTopicOutdated(0);
			}
			$this->roomMapper->update($room);
		}
	}
}
