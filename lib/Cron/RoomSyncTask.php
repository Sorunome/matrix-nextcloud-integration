<?php
namespace OCA\MatrixIntegration\Cron;

use OC\BackgroundJob\TimedJob;
use OCP\IUserManager;
use OCA\MatrixIntegration\Service\RoomSyncService;

class RoomSyncTask extends TimedJob {
	private $userManager;
	private $roomSyncService;

	public function __construct(
		IUserManager $userManager,
		RoomSyncService $roomSyncService
	) {
		$this->userManager = $userManager;
		$this->roomSyncService = $roomSyncService;
		$this->setInterval(10); // once every 10 seconds
	}

	protected function run($arguments) {
		$this->userManager->callForAllUsers(function ($user) {
			$cl = $this->roomSyncService->sync($user->getUID());
			$this->roomSyncService->updateCache($user->getUID(), $cl);
		});
	}
}
