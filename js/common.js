window.addEventListener('DOMContentLoaded', () => {
	let appName = 'matrix_integration';
	function url(path) {
		return OC.generateUrl('/apps/' + appName + path);
	}
	function closePopup() {
		$('#matrix-integration-room-picker').remove();
	}
	function roomPicker(title, callback) {
		let $el = $('<div id="matrix-integration-room-picker" class="popover">');
		$el.append($('<img class="loading icon-loading">'));
		$('body').append($el);

		let $filter = $('<input class="matrix-integration-room-picker-filter" type="text">');
		let $list = $('<div class="matrix-integration-room-picker-list">');
		$el.empty().append(
			$('<div class="popover__wrapper">').append(
				$('<div class="popover__inner">').append(
					$('<h5>').text('Close').css('cursor', 'pointer').click(closePopup),
					$('<h2>').text(title),
					$('<div>').append(
						$filter,
					),
					$list,
				),
			),
		);
		$.getJSON(url('/room_summary'), function(data) {
			function buildRoomList() {
				let filter = $filter.val() || '';
				let rooms = [];
				for (let d of data) {
					if (!filter || d.display_name.toLowerCase().includes(filter.toLowerCase())) {
						rooms.push($('<div>')
							.append(
								d.avatar_url ? (() => {
									let parts = d.avatar_url.split('/');
									let src = url(`/thumbnail/${parts[parts.length - 2]}/${parts[parts.length - 1]}`);
									return $('<img>').attr('src', src);
								})() : '',
								d.display_name
							).on('click', function (e) {
								e.preventDefault();
								e.stopPropagation();
								callback(d.room_id, d.display_name);
							}));
					}
				}
				$list.empty();
				$list.append(rooms);
			}
			
			$filter.on('keyup', function (e) {
				buildRoomList();
			});
			
			buildRoomList();
		});
	}

	if (OCA.Sharing && OCA.Sharing.ExternalLinkActions) {
		OCA.Sharing.ExternalLinkActions.registerAction({
			url: link => `matrixshare:${link}`,
			name: t('socialsharing_matrix', 'Share to Matrix'),
			icon: 'icon-matrix'
		});
		$(document).on('click', 'a[href^="matrixshare:"]', function (e) {
			e.preventDefault();
			e.stopPropagation();
			let shareUrl = $(this).attr('href').substr('matrixshare:'.length);
			roomPicker('Select a room to share into', function(roomId, name) {
				if (confirm('Are you sure you want to share the image to ' + name + '?')){
					let arr = shareUrl.split('/');
					let shareId = arr[arr.length - 1];
					$.ajax({
						url: url('/share'),
						method: 'POST',
						data: {
							room_id: roomId,
							share_id: shareId,
						},
						success: function() {
							closePopup();
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.error(errorThrown);
							alert('Error: ' + errorThrown);
						},
					});
				}
			});
		});
	}
});
