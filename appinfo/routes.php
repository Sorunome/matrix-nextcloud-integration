<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\MatrixIntegration\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */

use OC\Route\Router;
use OCA\MatrixIntegration\RouteConfig;
use OCA\MatrixIntegration\AppInfo\Application;

$application = \OC::$server->get(Application::class);
$router = $this;

// by using a modified RouteConfig to register the routes we can bypass the root url restrictions placed on most apps
$routeConfig = new RouteConfig($application->getContainer(), $router,  [
	'routes' => [
		// general personal matrix routes
		[
			'name' => 'matrix#whoami',
			'url' => '/whoami',
			'verb' => 'GET',
		],
		[
			'name' => 'matrix#login',
			'url' => '/login',
			'verb' => 'POST',
		],
		[
			'name' => 'matrix#logout',
			'url' => '/logout',
			'verb' => 'POST',
		],
		[
			'name' => 'matrix#roomSummary',
			'url' => '/room_summary',
			'verb' => 'GET',
		],
		[
			'name' => 'matrix#thumbnail',
			'url' => '/thumbnail/{mxc}',
			'verb' => 'GET',
			'requirements' => ['mxc' => '.+'],
		],
		[
			'name' => 'matrix#share',
			'url' => '/share',
			'verb' => 'POST',
		],
		// file sharing routes
		[
			'name' => 'fileShare#matrixDownload',
			'url' => '/_matrix/media/{ver}/download/{mxc}',
			'verb' => 'GET',
			'requirements' => ['mxc' => '.+'],
			'root' => '',
		],
		[
			'name' => 'fileShare#matrixThumbnail',
			'url' => '/_matrix/media/{ver}/thumbnail/{mxc}',
			'verb' => 'GET',
			'requirements' => ['mxc' => '.+'],
			'root' => '',
		],
		[
			'name' => 'fileShare#matrixEvent',
			'url' => '/_matrix/media/{ver}/event/{mxc}',
			'verb' => 'GET',
			'requirements' => ['mxc' => '.+'],
			'root' => '',
		],
		[
			'name' => 'fileShare#matrixWellKnown',
			'url' => '/.well-known/matrix/server',
			'verb' => 'GET',
			'root' => '',
		]
	],
]);
$routeConfig->register();
