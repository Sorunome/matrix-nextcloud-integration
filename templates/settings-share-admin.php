<?php
/** @var \OCP\IL10N $l */
/** @var array $_ */
script($_['appName'], 'settings-share-admin');
//style('federatedfilesharing', 'settings-admin');
?>

<div id="matrixSharingSettings" class="section" data-appname="<?php p($appName) ?>">
	<h2>
		<?php p($l->t('Matrix Sharing')); ?>
	</h2>

	<p>
		<label for="matrixShareSettingsDomain">
			<?php p($l->t('Domain')); ?>
		</label>
		<input type="text" name="share_domain" id="matrixShareSettingsDomain" value="<?php p($_['share_domain']); ?>" />
	</p>

	<p>
		<label for="matrixShareSettingsPrefix">
			<?php p($l->t('Prefix')); ?>
		</label>
		<input type="text" name="share_prefix" id="matrixShareSettingsPrefix" value="<?php p($_['share_prefix']); ?>" />
	</p>

	<p>
		<label for="matrixShareSettingsSuffix">
			<?php p($l->t('Suffix')); ?>
		</label>
		<input type="text" name="share_suffix" id="matrixShareSettingsSuffix" value="<?php p($_['share_suffix']); ?>" />
	</p>
</div>
